#ifndef LNLIST_H
#define LNLIST_H

#include <stdlib.h>
#include <memory.h>
#include <StopWatch.h>

/* Dynamic array, by Mikael Eiman 1999 */

template <class T>
class List
{
	protected:
		T ** array;
		int num_items;
		int array_size;
		bool delete_items;
		void SubSort(int, int); // used by sort
		
	public:
		List();
		~List();
		
		void operator += (T*);        // add an item
		void operator -= (T*);        // remove an item
		T * operator [] (int) const;   // access an item
		int NumItems();
		
		void Sort();
		void SetDeleteItems(bool d) { delete_items = d; };
		
		int IndexOf( T* );
		void InsertAt( T*, int );
};

template <class T>
List<T>::List()
:	num_items(0),
	array_size(4),
	delete_items(false)
{
	array = new T*[array_size];
}

template <class T>
List<T>::~List()
{
	if (delete_items)
	{
		for (int c=0;(*this)[c];c++)
			delete (*this)[c];
	}
	
	delete [] array;
}

template <class T>
void List<T>::operator += (T * item)
{
	if (num_items == array_size)
	{ // alloc new, bigger array
		array_size *= 2;
		T ** old = array;
		array = new T*[array_size];
		memset(array,0,sizeof(T*)*array_size);
		memcpy(array,old,sizeof(T*) * num_items);
		delete [] old;
	}
	array[num_items] = item;
	num_items++;
}

template <class T>
void List<T>::operator -= (T * item)
{
	for (int c=0;c<num_items;c++)
	{
		if (array[c] == item)
		{
			if (c < num_items-1)
			{
				memmove(&array[c],&array[c+1],sizeof(T*)*(num_items-c-1));
			}
			num_items--;
			return; // only remove ONE instance
		}
	}
}

template <class T>
T * List<T>::operator [] (int i) const
{
	if (i < num_items && i >= 0)
		return array[i];
	else
		return NULL;
}

template <class T>
int List<T>::NumItems()
{
	return num_items;
}

template <class T>
void List<T>::Sort()
{
	BStopWatch timer("LnList::Sort");
	if (num_items < 2)
		return; // no point in sorting a single item, is there?

	// sort array
	SubSort(0,num_items-1);
}

template <class T>
void List<T>::SubSort(int a, int b)
{ // stolen from one of the JDK demos
	if (a < b) 
	{ // sanity check
		T * divider = array[(a+b)/2];
		int lo = a, hi = b;
		while (lo <= hi) 
		{
			while ( (lo < b) && (*array[lo] < *divider) ) ++lo;
			while ( (hi > a) && (*array[hi] > *divider) ) --hi;
			if (lo <= hi) 
			{ // swap
				T * temp = array[lo]; 
				array[lo] = array[hi]; 
				array[hi] = temp;
				++lo; --hi;
			}
		}
		if (a < hi)
      	SubSort(a,hi);
		if (lo < b)
      	SubSort(lo,b);
	}
}

template <class T>
int List<T>::IndexOf( T * t )
{
	for ( int c=0; (*this)[c]; c++ )
	{
		if ( (*this)[c] == t )
			return c;
	}
	
	return -1;
}

template <class T>
void List<T>::InsertAt( T * t, int index )
{
	if ( index >= num_items )
	{ // normal add if at end
		*this += t;
		return;
	}
	
	if (num_items == array_size)
	{ // alloc new, bigger array
		array_size *= 2;
		T ** old = array;
		array = new T*[array_size];
		memset(array,0,sizeof(T*)*array_size);
		memcpy(array,old,sizeof(T*) * num_items);
		delete [] old;
	}
	
//	num_items - index;
	memmove(&array[index+1],&array[index],sizeof(T*)*(num_items-index));
	
	array[index] = t;
	
	num_items++;
}

#endif
