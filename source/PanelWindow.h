#ifndef PANEL_WINDOW_H
#define PANEL_WINDOW_H

#include <Window.h>

#include "LnList.h"

#include "IconView.h"
#include "HandleView.h"
#include "BubbleWindow.h"

#define PANEL_MODIFY_SIZE		'pMFY'
#define PANEL_SET_OPEN			'pSOP'
#define PANEL_TOGGLE_OPEN		'pTOP'
#define PANEL_SET_EDGE			'pSED'
#define PANEL_MOVE_TO			'pMTO'
#define PANEL_ADD_ICON			'pADD'
#define PANEL_REMOVE_ICON		'pREM'
#define PANEL_MOVE_ICON			'pMOV'
#define PANEL_SET_ICON_SIZE		'pISZ'
#define PANEL_SET_SCROLL_RATE	'pSCR'
#define PANEL_MOUSE_ENTER		'pMEN'
#define PANEL_MOUSE_EXIT		'pMEX'
#define PANEL_SET_EXPAND_ON_MOUSE_OVER	'pSEM'
#define PANEL_SET_THIN_HANDLE	'pSTH'
#define PANEL_SET_FLOATING		'pSFL'

class PanelWindow : public BWindow
{
	public:
		PanelWindow();
		PanelWindow( BMessage * );
		
		virtual ~PanelWindow();
		
		static BArchivable * Instantiate( BMessage* );
		virtual status_t Archive( BMessage *, bool ) const;
		
		virtual bool QuitRequested();
		
		virtual void ScreenChanged( BRect, color_space );
		
		enum ScreenEdge {
			TOP,
			BOTTOM,
			LEFT,
			RIGHT
		};
		
		void ModifySize();
		void PositionViews();
		
		virtual void MessageReceived( BMessage * );
		virtual void WorkspaceActivated(int32,bool);
		
		bigtime_t		PulseRate();
		
		bool		IsExpandOnMouseOver();
		bool		IsOpen();
		bigtime_t	AnimationDelay();	
		
	protected:
		void				CommonInit();
		
		bool				m_open;
		int32				m_size; // current size
		List<IconView>		m_icons;
		ScreenEdge			m_edge;
		HandleView			* m_handle;
		
		bigtime_t			m_pulse_rate;
		
		bool				m_small_icons;
		
		bool				m_thin_handle;
		
		thread_id			m_pulse_thread;
		
		bool				m_expand_on_mouse_over;
		
		bool				m_floating_window;
		
		BubbleStarter		* m_bubble_starter;
		
		BPoint				m_pos;
};

#endif

