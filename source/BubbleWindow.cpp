#include "BubbleWindow.h"
#include "PanelWindow.h"

#include <Application.h>
#include <Screen.h>
#include <Cursor.h>
#include <stdio.h>


const char NO_CURSOR[] = {
	16, // 16 pixels per side
	1,  // one bit per pixel hot - spot (x, y) bits transparency 
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ,0, 0, 0, 0, 0, 0, 0, 0,
	};


BubbleView::BubbleView( const char * text )
:	BView(
		BRect( 0, 0, 0, 0 ), "", B_FOLLOW_NONE, B_WILL_DRAW
	),
	m_text( text ),
	m_moves( 0 )
{
	BFont font;
	GetFont( &font );
	
	font.GetHeight( &m_fh );
	
	BRect bounds(0, 0, font.StringWidth(text),m_fh.ascent + m_fh.descent);
	bounds.InsetBy(-2,-2);
	
	ResizeTo( bounds.Width(), bounds.Height() );
	
	SetViewColor( 255, 255, 225, 0 );
}


BubbleView::~BubbleView()
{
	SetEventMask(0, 0);
}


void
BubbleView::AttachedToWindow()
{
	// get mouse events
	SetEventMask( B_POINTER_EVENTS, B_LOCK_WINDOW_FOCUS );
	// hide cursor
	BCursor cursor( NO_CURSOR );
	SetViewCursor( &cursor, true );
}


void
BubbleView::Draw( BRect )
{
	SetHighColor( 0, 0, 0 );
	SetLowColor( ViewColor() );
	
	StrokeRect( Bounds() );
	
	BPoint text_pos( 3, Bounds().bottom - m_fh.descent - 2 );
	
	DrawString( m_text.String(), text_pos );
}


void
BubbleView::MouseMoved( BPoint pos, uint32, const BMessage * )
{
	if ( m_moves++ > 3 )
		Window()->PostMessage( B_QUIT_REQUESTED );
}


BubbleWindow::BubbleWindow( BPoint where, const char * text )
:	BWindow(
		BRect( where.x, where.y - 1, where.x + 10, where.y + 10 ),
		"bubble",
		B_NO_BORDER_WINDOW_LOOK,
		B_FLOATING_ALL_WINDOW_FEEL,
		B_NOT_MOVABLE | B_NOT_RESIZABLE | B_NOT_ZOOMABLE
	)
{
	BubbleView * view = new BubbleView(text);
	
	AddChild( view );
	ResizeTo( view->Bounds().Width(), view->Bounds().Height() );
	MoveBy(0,-Bounds().Height() );
	
	BScreen screen( this );
	
	BRect scr = screen.Frame();
	BRect frame = Frame();
	
	if ( frame.top < 0 )
	{
		MoveTo( frame.left, 0 );
		frame = Frame();
	}
	
	if ( frame.left < 0 )
	{
		MoveTo( 0, frame.top );
		frame = Frame();
	}
	
	if ( frame.bottom >  scr.bottom )
	{
		MoveTo( frame.left, scr.bottom - frame.Height() );
		frame = Frame();
	}
	
	if ( frame.right >  scr.right )
	{
		MoveTo( scr.right - frame.Width(), frame.top );
		frame = Frame();
	}
}


BubbleWindow::~BubbleWindow()
{
}


// BUBBLE STARTER
BubbleStarter::BubbleStarter()
:	BView( BRect(-1,-1,-1,-1), "bubble starter", B_FOLLOW_NONE, B_PULSE_NEEDED ),
	m_last_view( NULL )
{
}


BubbleStarter::BubbleStarter( BMessage * msg )
:	BView( msg ),
	m_last_view( NULL )
{
}


BubbleStarter::~BubbleStarter()
{
}


BArchivable * 
BubbleStarter::Instantiate( BMessage * msg )
{
	if ( !validate_instantiation( msg, "BubbleStarter" ) )
		return NULL;
	return new BubbleStarter(msg);
}


void
BubbleStarter::MouseMoved( BPoint pos, uint32, const BMessage * )
{
	m_curr_pos = ConvertToScreen(pos);

	PanelWindow* window = dynamic_cast<PanelWindow*> (Window());
	if ( window && !window->Frame().Contains(m_curr_pos) )
	{
		m_last_view = NULL;

		if ( window->IsOpen() )
			window->PostMessage( PANEL_MOUSE_EXIT );
	}
}


void
BubbleStarter::AttachedToWindow()
{
	// get mouse events
	SetEventMask( B_POINTER_EVENTS, 0 );
}


void
BubbleStarter::Pulse()
{
	if ( m_last_pos == m_curr_pos )
	{
		if ( Window()->Frame().Contains( m_curr_pos ) )
		{
			BView * view = Window()->FindView( Window()->ConvertFromScreen(m_curr_pos) );
			
			if ( view != m_last_view )
			{
				m_last_view = view;
				
				BMessenger msgr( view );
				BMessage request(BUBBLE_TEXT_REQUEST);
//				printf("Requesting bubble text\n");
				msgr.SendMessage( &request, this );
			}
		}
	}
	
	m_last_pos = m_curr_pos;
}


void
BubbleStarter::MessageReceived( BMessage * msg )
{
	switch ( msg->what )
	{
		case BUBBLE_TEXT_REPLY:
		{
			if ( msg->FindString("text") ) 
			{
//				printf("  text is [%s]\n", msg->FindString("text"));
				BubbleWindow * win = new BubbleWindow( 
					m_curr_pos, 
					msg->FindString("text") 
				);
				win->Show();
			} else
			{
//				printf("  reply contains no text\n");
			}
		}	break;
		default:
			BView::MessageReceived( msg );
	}
}
